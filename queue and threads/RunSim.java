
public class RunSim implements Runnable {

	SimulationManager s;
	
	public RunSim(SimulationManager s)
	{
		this.s = s;
	}

	@Override
	public void run() {
		try {
			s.simulate();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
