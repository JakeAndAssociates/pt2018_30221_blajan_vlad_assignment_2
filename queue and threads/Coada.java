import java.util.LinkedList;
import java.util.Queue;

public class Coada 
{
	public Queue<Client> coada = new LinkedList<Client>();

	public void add(Client c)
	{
		coada.add(c);
	}
	
	public Client get()
	{
		return coada.remove();
	}
	
	public boolean isEmpty()
	{
		if(coada.isEmpty())
			return true;
		return false;
	}
	
	public int waitingTime()
	{
		int time = 0;
		
		for(Client c : coada)
			time += c.getServiceTime();
		
		return time;
	}
	
	public int length()
	{
		int length = 0;
		
		for(@SuppressWarnings("unused") Client c : coada)
			length++;
		
		return length;
	}
}	
