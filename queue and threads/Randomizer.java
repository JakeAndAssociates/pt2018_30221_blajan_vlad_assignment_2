import java.util.concurrent.ThreadLocalRandom;

public class Randomizer 
{
	private int min;
	private int max;
	
	public Randomizer(int min, int max)
	{
		this.min = min;
		this.max = max;
		
		if(this.min < 1)
			this.min = 1;
		
		if(this.max < this.min)
			this.max = this.min + 1;
	}
	
	public void randomize(BuyingClients bc, int number)
	{
		int clientId;
		int startingTime;
		int endingTime;
		int serviceTime;

		boolean method;
		
		for(int i = 0; i < number; i++)
		{
			clientId = i;
			startingTime = ThreadLocalRandom.current().nextInt(0, min + 1);
			serviceTime = ThreadLocalRandom.current().nextInt(min, max + 1);
			endingTime = ThreadLocalRandom.current().nextInt(startingTime, startingTime + max + 1);
			method = ThreadLocalRandom.current().nextInt(0, 2) == 1;
			
			bc.add(new Client(clientId, startingTime, endingTime, serviceTime, method));
		}
	}
	
}
