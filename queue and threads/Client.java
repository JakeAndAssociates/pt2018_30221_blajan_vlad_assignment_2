
public class Client implements Comparable<Object>
{
	private int clientId;
	private int startingTime;
	private int endingTime;
	private int serviceTime;

	private boolean method;
	
	public Client(int id, int start, int end, int service, boolean method)
	{
		clientId = id;
		startingTime = start;
		endingTime = end;
		serviceTime = service;
		this.method = method;
	}
	
	public void setClientId(int id)
	{
		clientId = id;
	}
	
	public int getClientId()
	{
		return clientId;
	}
	
	public void setStartingTime(int start)
	{
		startingTime = start;
	}
	
	public int getStartingTime()
	{
		return startingTime;
	}
	
	public void setEndingTime(int end)
	{
		endingTime = end;
	}
	
	public int getEndingTime()
	{
		return endingTime;
	}
	
	public void setServiceTime(int service)
	{
		serviceTime = service;
	}
	
	public int getServiceTime()
	{
		return serviceTime;
	}
	
	public void setMethod(boolean method)
	{
		this.method = method;
	}
	
	public boolean getMethod()
	{
		return method;
	}
	
	@Override
	public int compareTo(Object other) {
		if(endingTime == ((Client)other).getEndingTime())
				return startingTime - ((Client)other).getStartingTime();
		
		return endingTime - ((Client)other).getEndingTime();
	}
}
