import java.util.ArrayList;

public class SimulationManager implements Runnable
{
	public final boolean TIME = true;
	public final boolean LENGTH = false;
	
	//public final int MAX = 10;
	
	public int len;
	
	public BuyingClients waiting;
	public GUI gui;
	
	public ArrayList <Coada> cozi = new ArrayList <Coada>();
	
	public SimulationManager(GUI gui)
	{
		this.gui = gui;
		waiting = new BuyingClients(gui);
		
	}
	
	public void setQueues(int len)
	{	
		this.len = len;
		
		for(int i = 0; i < len; i++)
			cozi.add(new Coada());
	}
	
	@SuppressWarnings("unused")
	public int minQueue(boolean method)
	{
		int min = 0;
		
		for(int i = 1; i < len; i++)
		{
			if(method = TIME && cozi.get(min).waitingTime() > cozi.get(i).waitingTime())
				min = i;
			if(method = LENGTH && cozi.get(min).length() > cozi.get(min).length())
				min = i;
		}
		
		return min;
	}
	
	public void run()
	{
		long threadId = Thread.currentThread().getId();
		int itter = (int) (threadId) % len;
		
		while(true)
		{
			synchronized(cozi.get(itter)) {
			if(!cozi.get(itter).isEmpty())
			{
				try 
				{
					System.out.println(itter);
					Client c = cozi.get(itter).get();
					//TimeUnit.SECONDS.sleep(c.getServiceTime());
					Thread.sleep(c.getServiceTime() * 1000);
					gui.addLeavingMessage(c.getClientId());
					gui.queueUpdate(cozi);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
			
				if(waiting.isEmpty() && cozi.get(itter).isEmpty())
					break;
			}
			}
		}
	}

	public void simulate() throws InterruptedException
	{
		Thread[] t = new Thread[len];
		
		for(int i = 0; i < len; i++)
		{
			t[i] = new Thread(this);
			t[i].start();
		}
		
		while(!waiting.isEmpty())
		{
			gui.addClientWait(waiting);
			while(waiting.isQueing())
			{
				Client c = waiting.getClient();
				int min = minQueue(c.getMethod());
				
				gui.addQueingMessage(c.getClientId(), min);
				cozi.get(min).add(c);
				gui.queueUpdate(cozi);	
			}
			gui.addClientWait(waiting);
			Thread.sleep(1000);
			waiting.timePass();
		}
	}

	
}
