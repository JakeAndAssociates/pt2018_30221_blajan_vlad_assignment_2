import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class BuyingClients {
	
	public List<Client> buyingList = new ArrayList<Client>();
	public GUI gui;
	
	public BuyingClients(GUI gui)
	{
		this.gui = gui;
	}
	
	public void add(Client client)
	{
		buyingList.add(client);
		sortList();
	}
	
	public void sortList()
	{
		buyingList.sort(new Comparator<Client>()
		{	
			@Override
		    public int compare(Client o1, Client o2) {
		        return o1.compareTo(o2);
		    }
		});
	}
	
	public void timePass()
	{
		for(Client c : buyingList)
		{
			c.setStartingTime(c.getStartingTime() - 1);
			c.setEndingTime(c.getEndingTime() - 1);
			
			if(c.getStartingTime() == 0)
				gui.addArivingMessage(c.getClientId());
		}
	}
	
	public boolean isQueing()
	{
		if(buyingList.isEmpty())
			return false;
		if(buyingList.get(0).getEndingTime() == 0)
			return true;
		return false;
	}
	
	public Client getClient()
	{
		return buyingList.remove(0);
	}
	
	public boolean isEmpty()
	{
		if(buyingList.isEmpty())
			return true;
		return false;
	}
	
	public String toString()
	{
		String str = new String("");
		
		for(Client c: buyingList)
			if(c.getStartingTime() <= 0)
				str += c.getClientId() + " " + c.getEndingTime() + "\n";
		
		return str;
	}
}
