import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

public class GUI {

	private final int MAX = 900;
	private int len;
	
	private JFrame meniu = new JFrame("Meniu");
	private JPanel panel = new JPanel();
	
	private JPanel meniuPanel = new JPanel();
	private JPanel firstPanel = new JPanel();
	private JPanel secondPanel = new JPanel();
	private JPanel thirdPanel = new JPanel();
	private JPanel aux = new JPanel();
	
	private JLabel minLabel = new JLabel("min");
	private JLabel maxLabel = new JLabel("max");
	private JLabel nrClientiLabel = new JLabel("nrClienti");
	private JLabel queueNumberLabel = new JLabel("numar de cozi");
	private JTextField minText = new JTextField(10);
	private JTextField maxText = new JTextField(10);
	private JTextField queueNumberText = new JTextField(10);
	private JTextField nrClientiText = new JTextField(10);
	public JButton simulate = new JButton("simulate");
	public JButton run = new JButton("run");
	
	private JLabel maxWaitLabel = new JLabel("timp asteptare ora de varf");
	private JTextArea maxWaitText = new JTextArea(1, 10);
	private JLabel waitLabel = new JLabel("timp asteptare mediu");
	private JTextArea waitText = new JTextArea(1, 10);
	
	private JLabel clientWait = new JLabel("Clienti in Asteptare");
	private JTextArea clientWaitList = new JTextArea(20, 4);
	
	private JLabel[] queueName = new JLabel[MAX];
	private JTextArea[] queueList = new JTextArea[MAX];
	
	private JTextArea log = new JTextArea(16, 58);
	private JScrollPane scrollLog;
	
	
	public GUI()
	{
		meniu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		meniu.setSize(800, 600);
		
		meniuPanel.add(minLabel);
		meniuPanel.add(minText);
		meniuPanel.add(maxLabel);
		meniuPanel.add(maxText);
		meniuPanel.add(nrClientiLabel);
		meniuPanel.add(nrClientiText);
		meniuPanel.add(queueNumberLabel);
		meniuPanel.add(queueNumberText);
		meniuPanel.add(simulate);
		
		meniuPanel.setLayout(new BoxLayout(meniuPanel, BoxLayout.Y_AXIS));
		
		maxWaitText.setText("0");
		firstPanel.add(maxWaitLabel);
		firstPanel.add(maxWaitText);
		firstPanel.add(waitLabel);
		firstPanel.add(waitText);
		firstPanel.add(run);
		firstPanel.add(clientWait);
		firstPanel.add(clientWaitList);
		firstPanel.setLayout(new BoxLayout(firstPanel, BoxLayout.Y_AXIS));

		secondPanel.setLayout(new BoxLayout(secondPanel, BoxLayout.Y_AXIS));
		
		aux.add(firstPanel);
		aux.add(secondPanel);
		aux.setLayout(new FlowLayout());
		
		
		scrollLog = new JScrollPane(log);
		scrollLog.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		thirdPanel.add(scrollLog);
		thirdPanel.setLayout(new FlowLayout());
	
		panel.add(meniuPanel);
		panel.add(aux);
		panel.add(thirdPanel);
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		meniu.setContentPane(panel);
		
		
		aux.setVisible(false);
		thirdPanel.setVisible(false);
		meniu.setVisible(true);
	}
	
	SimulationManager s;
	
	public void buttonSim() throws InterruptedException
	{
		int min = Integer.parseInt(minText.getText());
		int max = Integer.parseInt(maxText.getText());
		int nrClienti = Integer.parseInt(nrClientiText.getText());
		len = Integer.parseInt(queueNumberText.getText());
		
		
		s = new SimulationManager(this);
		Randomizer r = new Randomizer(min, max);
		
		r.randomize(s.waiting, nrClienti);
		s.setQueues(len);
		
		meniuPanel.setVisible(false);
	
		for(int i = 0; i < len; i++)
		{
			queueName[i] = new JLabel("Queue " + i);
			queueList[i] = new JTextArea(2, 16);
			
			secondPanel.add(queueName[i]);
			secondPanel.add(queueList[i]);
		}
		
		aux.setVisible(true);
		thirdPanel.setVisible(true);
	}
	
	public void run()
	{
		RunSim rs = new RunSim(s);
		Thread t = new Thread(rs);
		
		t.start();
	}
	
	
	public void addArivingMessage(int clientId)
	{
		log.append("Client " + clientId + " has arrived\n");
	}
	
	public void addQueingMessage(int clientId, int queueId)
	{
		log.append("Client " + clientId + " has started queing at queue " + queueId + "\n");
	}
	
	public void addLeavingMessage(int clientId)
	{
		log.append("Client " + clientId + " has left\n");
	}
	
	public void addClientWait(BuyingClients bc)
	{
		if(bc.isEmpty())
			clientWaitList.setText("Toti clientii au terminat de cumparat");
		else
			clientWaitList.setText(bc.toString());
	}
	
	public void queueUpdate(ArrayList <Coada> qu)
	{
		int time = 0;
		for(int queueId = 0; queueId < this.len; queueId++)
		{
			queueList[queueId].setText("");
			int len = 0;
			int timp = 0;
			
			for(Client c : qu.get(queueId).coada)
			{
				queueList[queueId].append(c.getClientId() + " ");
				len++;
				timp += c.getServiceTime();
			}
			time += timp;
			queueName[queueId].setText("Queue " + queueId + " DurataAsteptare(" + timp + ") " + " Lungime(" + len + ")");
		}
		
		
		if(Integer.parseInt(maxWaitText.getText()) < time / this.len)
			maxWaitText.setText((time / this.len) + "");
		waitText.setText((time / this.len) + "");
	}
	
}
